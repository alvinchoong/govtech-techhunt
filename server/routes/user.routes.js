const express = require("express");
const router = express.Router();
const multer = require("multer");
const upload = multer({ dest: "tmp/" });

const userController = require("../controllers/user.controller.js");

const {
  createUserRules,
  getUsersRules,
  patchUserRules,
  validate,
} = require("../validations/user.validation.js");

// Upload Users via CSV file
router.post("/upload", upload.single("file"), userController.uploadCSV);

// Create a new User
router.post("/", createUserRules(), validate, userController.create);

// Retrieve all Users
router.get("/", getUsersRules(), validate, userController.getAll);

// Retrieve a single User by id
router.get("/:id", userController.getById);

// Patch a User by id
router.patch("/:id", patchUserRules(), validate, userController.update);

// Delete a User by id
router.delete("/:id", userController.destroy);

module.exports = router;