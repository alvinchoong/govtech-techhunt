const { check, validationResult } = require("express-validator");

// validtion for POST /users API
const createUserRules = () => {
  return [
    check("id")
      .trim()
      .escape()
      .not()
      .isEmpty()
      .withMessage("'id' cannot be empty")
      .isAlphanumeric()
      .withMessage("'id' must be alphanumeric"),
    check("name")
      .trim()
      .escape()
      .not()
      .isEmpty()
      .withMessage("'name' cannot be empty"),
    check("login")
      .trim()
      .escape()
      .not()
      .isEmpty()
      .withMessage("'login' cannot be empty")
      .isAlphanumeric()
      .withMessage("'login' must be alphanumeric"),
    check("salary")
      .toFloat()
      .isFloat({ min: 0, max: 99999999.99 })
      .withMessage("'salary' must be between 0 and 99999999.99"),
  ];
};

// validtion for PATCH /users API
const patchUserRules = () => {
  return [
    check("name")
      .optional()
      .trim()
      .escape()
      .not()
      .isEmpty()
      .withMessage("'name' cannot be empty"),
    check("login")
      .optional()
      .trim()
      .escape()
      .not()
      .isEmpty()
      .withMessage("'login' cannot be empty")
      .isAlphanumeric()
      .withMessage("'login' must be alphanumeric"),
    check("salary")
      .optional()
      .toFloat()
      .isFloat({ min: 0, max: 99999999.99 })
      .withMessage("'salary' must be between 0 and 99999999.99"),
  ];
};

// validation for GET /users API
const getUsersRules = () => {
  return [
    check("minSalary")
      .toFloat()
      .isFloat({ min: 0, max: 99999999.99 })
      .withMessage("'minSalary' must be between 0 and 99999999.99"),
    check("maxSalary")
      .toFloat()
      .isFloat({ min: 0, max: 99999999.99 })
      .withMessage("'maxSalary' must be between 0 and 99999999.99"),
    check("offset")
      .toInt()
      .isInt({ min: 0  })
      .withMessage("'offset' must be greater than equal to 0"),
    check("limit")
      .toInt()
      .isInt({ min: 0, max: 30 })
      .withMessage("'limit' must be between 0 and 30"),
    check("sort")
      .isIn([
        " id", " name", " login", " salary",
        "-id", "-name", "-login", "-salary",
      ])
      .withMessage("'sort' value is invalid"),
  ];
};

const validate = (req, res, next) => {
  const errors = validationResult(req);
  if (errors.isEmpty()) {
    return next();
  }
  const extractedErrors = [];
  errors.array().map((err) => extractedErrors.push(err.msg));

  return res.status(400).json({
    errors: extractedErrors,
  });
};

module.exports = {
  createUserRules,
  getUsersRules,
  patchUserRules,
  validate,
};
