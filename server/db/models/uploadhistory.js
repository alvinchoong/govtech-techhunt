'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class UploadHistory extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
    }
  };
  UploadHistory.init({
    status: DataTypes.STRING,
    original_filename: DataTypes.STRING,
    file_path: DataTypes.STRING
  }, {
    sequelize,
    modelName: 'UploadHistory',
  });
  return UploadHistory;
};