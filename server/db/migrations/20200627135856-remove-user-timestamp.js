'use strict';

module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.removeColumn("Users", "createdAt");
    await queryInterface.removeColumn("Users", "updatedAt");
  },

  down: async (queryInterface, Sequelize) => {
    await queryInterface.addColumn(
      "Users", 
      "createdAt", 
      {
        allowNull: true,
        type: Sequelize.DATE,
      }
    );
    
    await queryInterface.addColumn(
      "Users", 
      "updatedAt",
      {
        allowNull: true,
        type: Sequelize.DATE,
      },
    );
  }
};
