const sampleData = [
  {
    id: 'e0001',
    login: 'e0001',
    name: 'Amara Kuphal',
    salary: '2807.67'
  },
  {
    id: 'e0002',
    login: 'e0002',
    name: 'Wilford Windler',
    salary: '8464.25'
  },
  {
    id: 'e0003',
    login: 'e0003',
    name: 'Kristian DuBuque',
    salary: '5672.54'
  },
  {
    id: 'e0004',
    login: 'e0004',
    name: 'Christine Gleichner',
    salary: '6958.12'
  },
  {
    id: 'e0005',
    login: 'e0005',
    name: 'Izaiah Little',
    salary: '7955.54'
  },
  {
    id: 'e0006',
    login: 'e0006',
    name: 'Rick Cruickshank',
    salary: '3889.01'
  },
  {
    id: 'e0007', 
    login: 'e0007', 
    name: 'Mina Lind', 
    salary: '6007.47'
  },
  {
    id: 'e0008',
    login: 'e0008',
    name: 'Meghan Zboncak',
    salary: '4539.17'
  },
  {
    id: 'e0009',
    login: 'e0009',
    name: 'Milan Harris',
    salary: '2528.65'
  },
  {
    id: 'e0010',
    login: 'e0010',
    name: 'Lemuel Schultz',
    salary: '4108.39'
  },
  {
    id: 'e0011',
    login: 'e0011',
    name: 'Ona Stoltenberg',
    salary: '8050.90'
  },
  {
    id: 'e0012',
    login: 'e0012',
    name: "Percy O'Kon",
    salary: '5071.97'
  },
  {
    id: 'e0013',
    login: 'e0013',
    name: 'Sarah Rath',
    salary: '2077.20'
  },
  {
    id: 'e0014',
    login: 'e0014',
    name: 'Zelma Roberts',
    salary: '3316.84'
  },
  {
    id: 'e0015',
    login: 'e0015',
    name: 'Hallie Douglas',
    salary: '4003.15'
  },
  {
    id: 'e0016',
    login: 'e0016',
    name: 'Ashly VonRueden',
    salary: '2773.57'
  },
  {
    id: 'e0017',
    login: 'e0017',
    name: 'Susanna Harber',
    salary: '5912.88'
  },
  {
    id: 'e0018',
    login: 'e0018',
    name: 'Amparo Schoen',
    salary: '9638.26'
  },
  {
    id: 'e0019',
    login: 'e0019',
    name: 'Nadia Abshire',
    salary: '3176.81'
  },
  {
    id: 'e0020',
    login: 'e0020',
    name: 'Dedrick Quigley',
    salary: '9502.16'
  }
];

module.exports = sampleData;