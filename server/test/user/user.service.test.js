const chai = require("chai");
const chaiAsPromised = require("chai-as-promised");
chai.use(chaiAsPromised);

const userService = require("../../services/user.service");
const UserModel = require("../../db/models").User;
const { userFaker } = require("../helper");
const { expect } = chai;

const user1 = userFaker({ salary: 3000 });
const user2 = userFaker({ salary: 4000 });
const user3 = userFaker({ salary: 5000 });
const user4 = userFaker({ salary: 6000 });

describe("Test user services", () => {
  
  // // executed at the before every test
  beforeEach( async () => {
    await UserModel.destroy({
      truncate: true,
    });
    await UserModel.bulkCreate([user1, user2, user3, user4]);
  });

  describe("create()", () => {
    it("should create a user", async () => {
      const newUser = userFaker();
      const result = await userService.create(newUser);
      const data = result.toJSON();

      expect(data).to.be.an("object");
      expect(data.id).to.be.equal(newUser.id);
      expect(data.login).to.be.equal(newUser.login);
      expect(data.name).to.be.equal(newUser.name);
      expect(parseFloat(data.salary)).to.be.closeTo(parseFloat(newUser.salary), 0.01);
    });

    it("should create a user with lowercase id and login", async () => {
      const newUser = userFaker({ id: "UPPERCASEID", login: "UPPERCASELOGIN" });
      const result = await userService.create(newUser);
      const data = result.toJSON();

      expect(data).to.be.an("object");
      expect(data.id).to.be.equal(newUser.id.toLowerCase());
      expect(data.login).to.be.equal(newUser.login.toLowerCase());
      expect(data.name).to.be.equal(newUser.name);
      expect(parseFloat(data.salary)).to.be.closeTo(parseFloat(newUser.salary), 0.01);
    });

    it("should throw Error for duplicated id", async () => {
      const newUser = userFaker({ id: user1.id });
      const result = userService.create(newUser);

      await expect(result).to.eventually.be.rejectedWith(Error);
    });

    it("should throw Error for duplicated login", async () => {
      const newUser = userFaker({ login: user1.login });
      const result = userService.create(newUser);

      await expect(result).to.eventually.be.rejectedWith(Error);
    });
  });

  describe("getAll()", () => {
    it("should return results with length of 4", async () => {
      const minSalary = 0;
      const maxSalary = 6000;
      const sortColumn = "salary";
      const sortDirection = "ASC";

      const results = await userService.getAll(
        minSalary,
        maxSalary,
        sortColumn,
        sortDirection
      );
      expect(results).to.be.lengthOf(4);
    });

    it("should return results with length of 3", async () => {
      const minSalary = 0;
      const maxSalary = 5000;
      const sortColumn = "salary";
      const sortDirection = "ASC";

      const results = await userService.getAll(
        minSalary,
        maxSalary,
        sortColumn,
        sortDirection
      );
      expect(results).to.be.lengthOf(3);
    });

    it("should return results with length of 2", async () => {
      const minSalary = 0;
      const maxSalary = 4000;
      const sortColumn = "salary";
      const sortDirection = "ASC";

      const results = await userService.getAll(
        minSalary,
        maxSalary,
        sortColumn,
        sortDirection
      );
      expect(results).to.be.lengthOf(2);
    });
  });
  
  describe("getById()", () => {
    it("should get a user", async () => {
      const result = await userService.getById(user1.id);
      const data = result.toJSON();

      expect(data).to.be.an("object");
      expect(data.id).to.be.equal(user1.id);
      expect(data.login).to.be.equal(user1.login);
      expect(data.name).to.be.equal(user1.name);
      expect(parseFloat(data.salary)).to.be.closeTo(parseFloat(user1.salary), 0.01);
    });

    it("should return null for non existent user", async () => {
      const result = await userService.getById('12345');
      expect(result).to.be.null;
    });
  });

  describe("update()", () => {
    it("should return [1] after patching a user", async () => {
      const user = userFaker({ id: user1.id });
      const [result] = await userService.update(user.id, user);
      expect(result).to.be.equal(1);
    });

    it("should return [0] for non existent user", async () => {
      const user = userFaker();
      const [result] = await userService.update(user.id, user);
      expect(result).to.be.equal(0);
    });
  });

  describe("destroy()", () => {
    it("should return 1 after deleting a user", async () => {
      const result = await userService.destroy(user1.id);
      expect(result).to.be.equal(1);
    });

    it("should return 0 for non existent user", async () => {
      const result = await userService.destroy('12345');
      expect(result).to.be.equal(0);
    });
  });
  
  describe("updateOrCreate()", () => {
    it("should create a user if id dont exist", async () => {
      const newUser = userFaker();
      const result = await userService.updateOrCreate(newUser.id, newUser);
      const data = result.toJSON();
      
      expect(data).to.be.an("object");
      expect(data.id).to.be.equal(newUser.id);
      expect(data.login).to.be.equal(newUser.login);
      expect(data.name).to.be.equal(newUser.name);
      expect(parseFloat(data.salary)).to.be.closeTo(parseFloat(newUser.salary), 0.01);
    });

    it("should update user if id exist", async () => {
      const newUser = userFaker({ id: user1.id });
      const result = await userService.updateOrCreate(newUser.id, newUser);
      const data = result.toJSON();
      
      expect(data).to.be.an("object");
      expect(data.id).to.be.equal(newUser.id);
      expect(data.login).to.be.equal(newUser.login);
      expect(data.name).to.be.equal(newUser.name);
      expect(parseFloat(data.salary)).to.be.closeTo(parseFloat(newUser.salary), 0.01);
    });
  })

  describe("batchCreate()", () => {
    it("should rollback if data is invalid - e.g duplicate login", async () => {
      const newUserA = userFaker();
      const newUserB = userFaker();
      const newUserC = userFaker({ login: user1.login });

      const newUsers = [newUserA, newUserB, newUserC];

      const results = userService.batchCreate(newUsers);
      const user = userService.getById(newUserA.id);

      await expect(results).to.eventually.be.rejectedWith(Error);
      await expect(user).to.eventually.be.null;
    });

    it("should update and create users", async () => {
      const newUserA = userFaker();
      const newUserB = userFaker({ id: user1.id });
      const newUserC = userFaker();

      const newUsers = [newUserA, newUserB, newUserC];

      const results = await userService.batchCreate(newUsers);
      const updatedUser = results[1];
      
      expect(results).to.be.lengthOf(newUsers.length);
      expect(updatedUser).to.be.an("object");
      expect(updatedUser.id).to.be.equal(newUserB.id);
      expect(updatedUser.login).to.be.equal(newUserB.login);
      expect(updatedUser.name).to.be.equal(newUserB.name);
      expect(parseFloat(updatedUser.salary)).to.be.closeTo(parseFloat(newUserB.salary), 0.01);
    });
  });

  describe("getIdByLogin()", () => {
    it("should get a user", async () => {
      const result = await userService.getIdByLogin(user1.login);
      const data = result.toJSON();

      expect(data).to.be.an("object");
      expect(data.id).to.be.equal(user1.id);
    });

    it("should return null for non existent login", async () => {
      const result = await userService.getIdByLogin('12345');
      expect(result).to.be.null;
    });
  });

});