const chai = require("chai");
const chaiHttp = require("chai-http");
chai.use(chaiHttp);

const app = require("../../index");
const UserModel = require("../../db/models").User;
const { userFaker } = require("../helper");
const { expect } = chai;

const user1 = userFaker({ salary: 3000 });
const user2 = userFaker({ salary: 4000 });
const user3 = userFaker({ salary: 5000 });
const user4 = userFaker({ salary: 6000 });

describe("Test /users APIs", () => {
  
  // // executed at the before every test
  beforeEach( async () => {
    await UserModel.destroy({
      truncate: true,
    });
    await UserModel.create(user1);
    await UserModel.create(user2);
    await UserModel.create(user3);
    await UserModel.create(user4);
  });

  describe("POST /users", () => {
    it("should create a user", (done) => {
      const newUser = userFaker();
      chai
        .request(app)
        .post("/users")
        .send(newUser)
        .end((err, res) => {
          expect(res).to.have.status(201);
          expect(res).to.be.an("object");
          expect(res.body.id).to.be.equal(newUser.id);
          expect(res.body.name).to.be.equal(newUser.name);
          expect(res.body.login).to.be.equal(newUser.login);
          expect(parseFloat(res.body.salary)).to.be.closeTo(parseFloat(newUser.salary), 0.01);
          done();
        });
    });

    it("should return status 409 for duplicate id", (done) => {
      const newUser = userFaker({ id: user1.id });
      chai
        .request(app)
        .post("/users")
        .send(newUser)
        .end((err, res) => {
          expect(res).to.have.status(409);
          expect(res).to.be.an("object");
          done();
        });
    });

    it("should return status 409 for duplicate login", (done) => {
      const newUser = userFaker({ login: user1.login });
      chai
        .request(app)
        .post("/users")
        .send(newUser)
        .end((err, res) => {
          expect(res).to.have.status(409);
          expect(res).to.be.an("object");
          done();
        });
    });
  });

  describe("GET /users/:id", () => {
    it("should get a user", (done) => {
      chai
        .request(app)
        .get(`/users/${user1.id}`)
        .end((err, res) => {
          expect(res).to.have.status(200);
          expect(res).to.be.an("object");
          expect(res.body.id).to.be.equal(user1.id);
          expect(res.body.name).to.be.equal(user1.name);
          expect(res.body.login).to.be.equal(user1.login);
          expect(parseFloat(res.body.salary)).to.be.closeTo(parseFloat(user1.salary), 0.01);
          done();
        });
    });

    it("should return 404 for non existent user", (done) => {
      chai
        .request(app)
        .get("/users/12345")
        .end((err, res) => {
          expect(res).to.have.status(404);
          expect(res.body).to.have.property("errors");
          expect(res.body.errors).to.be.lengthOf(1);
          expect(res.body.errors[0]).to.be.equal("User does not exists");
          done();
        });
    });
  });
  
  describe("GET /users", () => {
    it("should return results with length of 4", (done) => {
      chai
        .request(app)
        .get("/users?minSalary=0&maxSalary=10000&offset=0&limit=30&sort=+name")
        .end((err, res) => {
          expect(res).to.have.status(200);
          expect(res).to.be.an("object");
          expect(res.body).to.have.property("results");
          expect(res.body.results).to.be.an("array");
          expect(res.body.results).to.be.lengthOf(4);
          done();
        });
    });

    it("should return results with length of 3", (done) => {
      chai
        .request(app)
        .get("/users?minSalary=0&maxSalary=5000&offset=0&limit=30&sort=+name")
        .end((err, res) => {
          expect(res).to.have.status(200);
          expect(res).to.be.an("object");
          expect(res.body).to.have.property("results");
          expect(res.body.results).to.be.an("array");
          expect(res.body.results).to.be.lengthOf(3);
          done();
        });
    });

    it("should return results with length of 2", (done) => {
      chai
        .request(app)
        .get("/users?minSalary=0&maxSalary=10000&offset=0&limit=2&sort=+name")
        .end((err, res) => {
          expect(res).to.have.status(200);
          expect(res).to.be.an("object");
          expect(res.body).to.have.property("results");
          expect(res.body.results).to.be.an("array");
          expect(res.body.results).to.be.lengthOf(2);
          done();
        });
    });

    it("should return status 400 due to missing sort param", (done) => {
      chai
        .request(app)
        .get("/users?minSalary=0&maxSalary=4000&offset=0&limit=30")
        .end((err, res) => {
          expect(res).to.have.status(400);
          expect(res).to.be.an("object");
          expect(res.body).to.have.property("errors");
          expect(res.body.errors).to.be.an("array");
          done();
        });
    });

    it("should return status 400 due to missing limit param", (done) => {
      chai
        .request(app)
        .get("/users?minSalary=0&maxSalary=4000&offset=0&sort=+name")
        .end((err, res) => {
          expect(res).to.have.status(400);
          expect(res).to.be.an("object");
          expect(res.body).to.have.property("errors");
          expect(res.body.errors).to.be.an("array");
          done();
        });
    });

    it("should return status 400 due to missing offset param", (done) => {
      chai
        .request(app)
        .get("/users?minSalary=0&maxSalary=4000&limit=30&sort=+name")
        .end((err, res) => {
          expect(res).to.have.status(400);
          expect(res).to.be.an("object");
          expect(res.body).to.have.property("errors");
          expect(res.body.errors).to.be.an("array");
          done();
        });
    });

    it("should return status 400 due to missing maxSalary param", (done) => {
      chai
        .request(app)
        .get("/users?minSalary=0&offset=0&limit=30&sort=+name")
        .end((err, res) => {
          expect(res).to.have.status(400);
          expect(res).to.be.an("object");
          expect(res.body).to.have.property("errors");
          expect(res.body.errors).to.be.an("array");
          done();
        });
    });

    it("should return status 400 due to missing minSalary param", (done) => {
      chai
        .request(app)
        .get("/users?maxSalary=4000&offset=0&limit=30&sort=+name")
        .end((err, res) => {
          expect(res).to.have.status(400);
          expect(res).to.be.an("object");
          expect(res.body).to.have.property("errors");
          expect(res.body.errors).to.be.an("array");
          done();
        });
    });
  });

  describe("PATCH /users/:id", () => {
    it("should patch a user", (done) => {
      const user = userFaker({ id: user1.id, login: user1.login });

      chai
        .request(app)
        .patch(`/users/${user.id}`)
        .send(user)
        .end((err, res) => {
          expect(res).to.have.status(200);
          expect(res).to.be.an("object");
          expect(res.body.id).to.be.equal(user.id);
          expect(res.body.name).to.be.equal(user.name);
          expect(res.body.login).to.be.equal(user.login);
          expect(parseFloat(res.body.salary)).to.be.closeTo(parseFloat(user.salary), 0.01);
          done();
        });
    });

    it("should return 404 for non existent user", (done) => {
      const user = userFaker();

      chai
        .request(app)
        .patch(`/users/12345`)
        .send(user)
        .end((err, res) => {
          expect(res).to.have.status(404);
          expect(res.body).to.have.property("errors");
          expect(res.body.errors).to.be.lengthOf(1);
          expect(res.body.errors[0]).to.be.equal("User does not exists");
          done();
        });
    });    
  });

  describe("DELETE /users/:id", () => {
    it("should delete a user", (done) => {
      chai
        .request(app)
        .delete(`/users/${user1.id}`)
        .end((err, res) => {
          expect(res).to.have.status(204);
          done();
        });
    });

    it("should return 404 for non existent user", (done) => {
      chai
        .request(app)
        .delete("/users/12345")
        .end((err, res) => {
          expect(res).to.have.status(404);
          expect(res.body).to.have.property("errors");
          expect(res.body.errors).to.be.lengthOf(1);
          expect(res.body.errors[0]).to.be.equal("User does not exists");
          done();
        });
    });
  });
});