const chai = require("chai");
const chaiAsPromised = require("chai-as-promised");
chai.use(chaiAsPromised);

const userController = require("../../controllers/user.controller");
const UserModel = require("../../db/models").User;
const { userFaker } = require("../helper");
const { expect } = chai;

describe("Test user controller", () => {
  // // executed at the before every test
  beforeEach(async () => {
    await UserModel.destroy({
      truncate: true,
    });
  });

  describe("parseCSV()", () => {
    it("should parse successfully", async () => {
      const results = await userController.parseCSV('test/files/valid-file.csv');
      expect(results).to.be.an("array");
      expect(results).to.be.lengthOf(5);
    });

    it("should parse rows without #", async () => {
      const results = await userController.parseCSV('test/files/valid-file-with-comment.csv');
      expect(results).to.be.an("array");
      expect(results).to.be.lengthOf(5);
    });

    it("should reject if file is empty", async () => {
      const results = userController.parseCSV('test/files/empty-file.csv');
      await expect(results).to.eventually.be.rejected;
    });

    it("should reject if salary is invalid", async () => {
      const results = userController.parseCSV('test/files/file-with-negative-salary.csv');
      await expect(results).to.eventually.be.rejected;
    });

    it("should reject if number of columns is incorrect", async () => {
      const results = userController.parseCSV('test/files/file-with-invalid-column.csv');
      await expect(results).to.eventually.be.rejected;
    });

    it("should reject if file has duplicated id", async () => {
      const results = userController.parseCSV('test/files/file-with-duplicate-id.csv');
      await expect(results).to.eventually.be.rejected;
    });

    it("should reject if file has duplicated login", async () => {
      const results = userController.parseCSV('test/files/file-with-duplicate-login.csv');
      await expect(results).to.eventually.be.rejected;
    });


  });

  describe("validateCsvRow()", () => {
    it("should return true if validation pass", (done) => {
      const data = userFaker();
      const result = userController.validateCsvRow(data);
      expect(result).to.be.true;
      done();
    });

    it("should return false if id is not alphanumeric", (done) => {
      const data = userFaker({ id: '!!#%#!@#' });
      const result = userController.validateCsvRow(data);
      expect(result).to.be.false;
      done();
    });

    it("should return false if login is not alphanumeric", (done) => {
      const data = userFaker({ login: '!!#%#!@#' });
      const result = userController.validateCsvRow(data);
      expect(result).to.be.false;
      done();
    });

    it("should return false if name is empty", (done) => {
      const data = userFaker({ name: '' });
      const result = userController.validateCsvRow(data);
      expect(result).to.be.false;
      done();
    });

    
    it("should return false if salary is negative", (done) => {
      const data = userFaker({ salary: -100 });
      const result = userController.validateCsvRow(data);
      expect(result).to.be.false;
      done();
    });

    it("should return false if salary is not a number", (done) => {
      const data = userFaker({ salary: "five thousand" });
      const result = userController.validateCsvRow(data);
      expect(result).to.be.false;
      done();
    });
  });

});