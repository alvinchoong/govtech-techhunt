const chai = require("chai");
const chaiAsPromised = require("chai-as-promised");
chai.use(chaiAsPromised);

const { uploadHistoryFaker } = require("../helper");
const UploadHistoryModel = require("../../db/models").UploadHistory;
const uploadhistoryService = require("../../services/uploadhistory.service");
const { expect } = chai;

const uploadHistory1 = uploadHistoryFaker({ status: 'SUCCESS' });
const uploadHistory2 = uploadHistoryFaker({ status: 'SUCCESS' });
const uploadHistory3 = uploadHistoryFaker({ status: 'PROCESSING' });

describe("Test upload history services", () => {
  // // executed at the before every test
  beforeEach(async () => {
    await UploadHistoryModel.destroy({
      truncate: true,
    });
    await UploadHistoryModel.bulkCreate([uploadHistory1, uploadHistory2, uploadHistory3]);
  });

  describe("create()", () => {
    it("should create a record", async () => {
      const uploadHistory = uploadHistoryFaker();
      const result = await uploadhistoryService.create(uploadHistory);
      const data = result.toJSON();

      expect(data).to.be.an("object");
      expect(data.status).to.be.equal(uploadHistory.status);
      expect(data.original_filename).to.be.equal(uploadHistory.original_filename);
      expect(data.file_path).to.be.equal(uploadHistory.file_path);
      expect(data.status).to.be.equal(uploadHistory.status);
    });
  });

  describe("countByStatus()", () => {
    it("should return 2 for SUCCESS", async () => {
      const result = await uploadhistoryService.countByStatus("SUCCESS");
      expect(result).to.be.equal(2);
    });

    it("should return 1 for PROCESSING", async () => {
      const result = await uploadhistoryService.countByStatus("PROCESSING");
      expect(result).to.be.equal(1);
    });

    it("should return 0 for FAILED", async () => {
      const result = await uploadhistoryService.countByStatus("FAILED");
      expect(result).to.be.equal(0);
    });
  });

  describe("updateStatus()", () => {
    it("should update record in database", async () => {
      const uploadHistory = uploadHistoryFaker({ status: "PROCESSING" });
      const result = await uploadhistoryService.create(uploadHistory);
      const [updated] = await uploadhistoryService.updateStatus(result.id, "FAILED"); 
      expect(updated).to.be.equal(1);
    });
  });

});