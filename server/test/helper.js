const faker = require('faker');

// simple function to generate user data
const userFaker = (obj = {}) => {
  const max = 10000;
  const min = 2000;

  const {
    id = faker.random.alphaNumeric(6),
    login = faker.random.alphaNumeric(6),
    name = `${faker.name.firstName()} ${faker.name.lastName()}`,
    salary = faker.finance.amount(min, max, 2)
  } = obj;

  return { id, login, name, salary };
}

// simple function to generate upload history data
const uploadHistoryFaker = (obj = {}) => {
  const filename = `${faker.system.fileName()}.csv`;
  const statuses = [`SUCCESS`, `PROCESSING`, `FAILED`];

  const {
    status = statuses[faker.random.number({ 'min': 0, 'max': 2 })],
    original_filename = filename,
    file_path = `tmp/${filename}`
  } = obj;

  return { status, original_filename, file_path };
}

module.exports = {
  userFaker,
  uploadHistoryFaker
};