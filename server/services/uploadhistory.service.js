const db = require('../db/models');
const UploadHistory = db.UploadHistory;

const create = async (data, options = {}) => {
  return await UploadHistory.create(data, options);
}

const countByStatus = async (status, options = {}) => {
  return await UploadHistory.count({
    where: { status },
    ...options
  });
}

const updateStatus = async (id, status, options = {}) => {
  return await UploadHistory.update({status}, {
    where: { id },
    ...options
  });
}

module.exports = {
  create,
  countByStatus,
  updateStatus
};