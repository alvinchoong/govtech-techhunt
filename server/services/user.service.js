const db = require('../db/models');
const UserModel = db.User;
const { Op } = require("sequelize");

const create = async (userData, options={}) => {
  const data = formatData(userData);
  return await UserModel.create(data, options);
}

const getAll = async (
  minSalary,
  maxSalary,
  sortColumn,
  sortDirection,
  options={}) => {
  
  // filter users based on salary range (inclusive)
  const whereClause = {
    salary: {
      [Op.between]: [minSalary, maxSalary]
    },
  };

  // order users based on column and direction
  const orderClause = [[sortColumn, sortDirection]];

  return await UserModel.findAll({
    where: whereClause,
    order: orderClause,
    ...options
  });
}

const getById = async (id, options={}) => {
  return await UserModel.findOne({
    where: { id },
    ...options
  });
}

const update = async (id, userData, options={}) => {
  const data = formatData(userData);
  return await UserModel.update(data, {
    where: { id },
    ...options
  });
}

const destroy = async (id, options={}) => {
  return await UserModel.destroy({
    where: { id },
    ...options
  });
}

const updateOrCreate = async (id, userData, options={}) => {
  const data = formatData(userData);
  return await UserModel.findOne({ 
    where: { id },
    ...options
  }).then(async (obj) => {
    // update
    if (obj) return await obj.update(data);
    // insert
    return await UserModel.create(data);
  });
}

const getIdByLogin = async (login, options={}) => {
  return await UserModel.findOne({
    attributes: ['id'],
    where: { login },
    ...options
  });
}

const batchCreate = async (usersData) => {
  const t = await db.sequelize.transaction();
  const results = [];

  try {
    for (const userData of usersData) {
      const user = await updateOrCreate(userData.id, userData, { transaction: t });
      results.push(user);
    }
    await t.commit();
    return results;
  } catch (error) {
    await t.rollback();
    throw new Error(error.message);
  }
}

const formatData = (userData) => {
  const data = {...userData};
  if (data.id) {
    data.id = data.id.toLowerCase();
  } 
  if (data.login) {
    data.login = data.login.toLowerCase();
  }
  if (data.salary) {  
    data.salary = Math.floor(parseFloat(data.salary) * 100) / 100
  }
  return data;
}

module.exports = {
  create,
  getAll,
  getById,
  update,
  destroy,
  updateOrCreate,
  batchCreate,
  getIdByLogin
};