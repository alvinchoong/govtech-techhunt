const userService = require('../services/user.service');
const uploadHistoryService = require('../services/uploadhistory.service');

const csv = require("@fast-csv/parse");
const fs = require('fs');

// Parse data from CSV and create users
const uploadCSV = async (req, res) => {
  if (!req.file) return res.status(429).send({ errors: ["Please upload a file"] });

  let status = 'PROCESSING';
  let uploadId;
  
  try {
    const uploadData = {
      status,
      original_filename: req.file.originalname,
      file_path: req.file.path
    };

    const uploadHistory = await uploadHistoryService.create(uploadData);
    uploadId = uploadHistory.id; 

    const count = await uploadHistoryService.countByStatus('PROCESSING');
    if (count - 1) {
      status = "FAILED";
      return res.status(429).send({ errors: ["Another file is being processed"] });
    }

    const usersData = await parseCSV(req.file.path);

    const duplicateLogins = new Map();
    let tmpUsersData = [];

    for (let i = 0; i < usersData.length; i++) {
      const userData = usersData[i];
      const user = await userService.getIdByLogin(userData.login);

      if (!user || user.id == userData.id) continue;

      if (!duplicateLogins.has(user.id)) {
        duplicateLogins.set(userData.id, user.id);
      } else if (duplicateLogins.get(user.id) === userData.id) { // check if theres a swap in login
        tmpUsersData.push({
          id: userData.id,
          login: `${Date.now()}${userData.id}`
        });

        duplicateLogins.delete(user.id);
      }
    }

    if (duplicateLogins.size) {
      status = "FAILED";
      return res.status(409).send({ errors: ["Duplicate login detected"] })
    };

    const results = await userService.batchCreate(tmpUsersData.concat(usersData));
    status = "SUCCESS";
    return res.status(200).json(results);
  } catch (error) {
    status = "FAILED";
    if (error.errors) { // validation error
      return res.status(400).send(error);
    }
    return res.status(500).send(error);
  } finally {
    if (uploadId) await uploadHistoryService.updateStatus(uploadId, status);
  }
};

// Create and save a new user
const create = async (req, res) => {
  try {
    const userData = req.body;
    const user = await userService.create(userData);
    return res.status(201).json(user)
  } catch (error) {
    if(error.original && error.original.code === 'ER_DUP_ENTRY') {
      return res.status(409).json({ errors: [error.original.message] });
    }
    
    return res.status(500).send(error);
  }
};

// Retrieve and return all users from the database.
const getAll = async (req, res) => {
  try {
    const {
      minSalary,
      maxSalary,
      offset,
      limit,
      sort
    } = req.query;

    const sortDirection = sort.slice(0, 1) === "-" ? "DESC" : "ASC";
    const sortColumn = sort.slice(1);
    
    const users = await userService.getAll(minSalary, maxSalary, sortColumn, sortDirection);

    const count = users.length;
    const results = users.slice(offset, offset + limit);

    return res.status(200).json({ results, count });
  } catch (error) {
    return res.status(500).send(error);
  }
};

// Find a user by the id
const getById = async (req, res) => {
  try {
    const { id } = req.params;
    const user = await userService.getById(id);
    if (user) {
      return res.status(200).json(user);
    }
    return res.status(404).send({ errors: ['User does not exists'] });
  } catch (error) {
    return res.status(500).send(error);
  }
};

// Update a user identified by the id in the request
const update = async (req, res) => {
  try {
    const { id } = req.params;
    const userData = req.body;
    delete userData.id;

    const hasUser = await userService.getById(id);
    if(!hasUser) {
      return res.status(404).send({ errors: ['User does not exists'] });
    }

    if(userData.login) {
      const user = await userService.getIdByLogin(userData.login);

      if (user && user.id != id) {
        return res.status(409).send({ errors: ["Duplicate login detected"] });
      }
    }
    
    await userService.update(id, userData);

    const user = await userService.getById(id);
    return res.status(200).json( user );
  } catch (error) {
    console.log(error);
    return res.status(500).send(error);
  }
};

// Delete a user with the specified id in the request
const destroy = async (req, res) => {
  try {
    const { id } = req.params;
    const deleted = await userService.destroy(id);
    
    if (deleted) {
      return res.status(204).send("User deleted");
    }
    return res.status(404).send({ errors: ['User does not exists'] });
  } catch (error) {
    return res.status(500).send(error);
  }
};

// parse CSV file to JSON
const parseCSV = async (pathToFile) => {
  return new Promise((resolve, reject) => {
    const usersData = [];

    const userIdMap = new Map();
    const userLoginMap = new Map();
    
    const parseOptions = {
      headers: ["id", "login", "name", "salary"],
      renameHeaders: true,
      trim: true,
      strictColumnHandling: true,
      ignoreEmpty: false,
      comment: "#",
    };

    fs.createReadStream(pathToFile)
      .pipe(csv.parse(parseOptions))
      .validate(validateCsvRow)
      .on("error", (error) => {
        reject(error.message);
      })
      .on("data-invalid", (row, rowNumber) => {
        reject({ errors: [`Invalid data at row ${rowNumber+1}`] })
      })
      .on("data", (row) => {
        row.id = row.id.toLowerCase();
        row.login = row.login.toLowerCase();
        
        // check for duplicated id in file
        if (userIdMap.has(row.id)) {
          reject({ errors: [`Invalid fild. Duplicated id detected`] });
        }
        // check for duplicated login in file
        if (userLoginMap.has(row.login)) {
          reject({ errors: [`Invalid fild. Duplicated login detected`] });
        }
        
        userIdMap.set(row.id, true);
        userLoginMap.set(row.login, true);
        usersData.push(row);
      })
      .on("end", () => {
        if (!usersData.length) reject({ errors: [`File has no valid rows`] });
        resolve(usersData);
      });
  });
}

// validate CSV data
const validateCsvRow = (data) => {
  // validate id is alphanumeric
  if (!data.id.match(/^[a-z0-9]+$/i)) {
    return false;
  }
  // validate login is alphanumeric
  if (!data.login.match(/^[a-z0-9]+$/i)) {
    return false;
  }

  // validate name length > 0
  if (!data.name.length) {
    return false;
  }

  // validate salary is positive and less than 100000000
  const salary = parseFloat(data.salary);
  if (isNaN(salary) || salary < 0 || salary >= 100000000) {
    return false;
  }

  return true;
};

module.exports = {
  uploadCSV,
  create,
  getAll,
  getById,
  update,
  destroy,
  parseCSV,
  validateCsvRow
}