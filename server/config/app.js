import express from "express";
import bodyParser from "body-parser";

const app = express();

const cors = require("cors");
const usersRoute = require("../routes/user.routes");

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));
app.use(cors());

app.use('/users', usersRoute);

export default app;
