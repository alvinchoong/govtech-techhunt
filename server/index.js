import app from "./config/app";

const PORT = process.env.PORT || 8000;

app.listen(PORT, () => {
  console.log(`app running on port ${PORT}`);
});

module.exports = app;