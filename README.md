# Alvin Choong - GovTech TechHunt 2020

## Table of Contents

- [About the Project](#markdown-header-about-the-project)
- [Built With](#markdown-header-built-with)
- [Getting Started](#markdown-header-getting-started)
    - [Prerequisites](#markdown-header-prerequisites)
    - [Setup: Backend](#markdown-header-setup-backend)
    - [Setup: Frontend](#markdown-header-setup-frontend)
- [Running Test](#markdown-header-running-test)
- [Assumptions & Notes](#markdown-header-assumptions-notes)
    - [User Story 1: Upload Users](#markdown-header-user-story-1-upload-users)
    - [User Story 2: Employee Dashboard](#markdown-header-user-story-2-employee-dashboard)
    - [User Story 3: CRUD](#markdown-header-user-story-3-crud)
    - [User Story 4: Better UX When Uploading Large CSV Files](#markdown-header-user-story-4-better-ux-when-uploading-large-csv-files)
    - [User Story 5: UI Localization](#markdown-header-user-story-5-ui-localization)
- [Contact](#contact)

## About the Project
Take home assignment for GovTech TechHunt 2020. 

The project consist of two portion, [Frontend](client/) and [Backend](server/)

## Built With

- Backend : [Node.js](https://nodejs.org/) / [Express](https://expressjs.com/)
- Frontend : [React](https://reactjs.org/) / [Material-UI](https://material-ui.com/)
- Database : [MySQL](https://www.mysql.com/)
- Test : [Mocha](https://mochajs.org/) / [Chai](https://www.chaijs.com/)

## Getting Started

To get a local copy up and running follow these simple steps.

### Prerequisites

1\. You will need

- [Node.js](https://nodejs.org/)
- [npm](https://www.npmjs.com/)

2\. Clone the project

```
git clone https://alvinchoong@bitbucket.org/alvinchoong/govtech-techhunt.git
```

3\. Navigate to project directory.

```
cd govtech-techhunt
```

### Setup: Backend
 
1\. In your terminal, navigate to the server directory.

```
cd server
```

2\. Install dependencies

```
npm install
```

3\. Copy configuration

```
cp .env.example .env
```

4\. Update .env file. For example:

```
DB_HOST=127.0.0.1
DB_USER=root
DB_PASSWORD=root
DB_NAME=govtech_techhunt
PORT=8000
```

5a\. Setup database

```
npx sequelize db:create
npx sequelize db:migrate
```

5b. Seed database with sample data (optional)

```
npx sequelize db:seed:all 
```

6\. Run application

```
npm run dev
```

### Setup: Frontend

1\. In another terminal, navigate to the client directory.

```
cd client
```

2\. Install dependencies

```
npm install
```

3\. Run application

```
npm start
```

## Running Test

Step 1 to 4 is only required for first setup

1\. In your terminal, navigate to the server directory.

```
cd server
```

2\. Update .env.test file. For example:

```
DB_HOST=127.0.0.1
DB_USER=root
DB_PASSWORD=root
DB_NAME=govtech_techhunt_test
PORT=8001
```

3\. Setup database for test environment

```
export NODE_ENV=test
npx sequelize db:create
npx sequelize db:migrate
```

4\. Run test

```
npm test
```

## Assumptions & Notes

### User Story 1: Upload Users

Validations:

- `id` and `login` should be alphanumeric
- `salary` should be between `0.00` and `99999999.99`
- All columns must not be empty
- Each row should have exactly 4 columns

Assumptions:

- File should be in csv
- First row is for header (`id`, `login`, `name`, `salary`)
- Should omit any row starting with `#`
- Reject entire file if any row fails validition
- `id` and `login` to be stored in lowercase
- `salary` to drop any digit after second decimal place

Notes:

- Concurrent upload is not supported.
- `login` swap between users is supported.
- If duplicated `login` is not resolved, HTTP 409 status is returned
- If file has duplicated `id` or `login`, HTTP 400 status is returned
- If another file is being processed, HTTP 429 status is returned
- Each upload is tracked and stored in database

### User Story 2: Employee Dashboard

Validations:

- `minSalary` and `maxSalary` should be between `0.00` and `99999999.99`
- `offset` should be greater than or equal to 0
- `limit` should be between 0 and 30
- `sort` to have value of one of the following (`id`, `name`, `login`, `salary`) with prefixed of `+` or `-`

Assumptions:

- Requires all 5 params to be present (`minSalary`, `maxSalary`, `offset`, `limit`, `sort`)
- Salary filter is inclusive of value
- Sort order, `-` represents `desc` and `+` represents `asc`

Notes:

- Successful response should return data in this format
```
{
    results: [{id:e0001, name: John, login: john, salary: 1000.00}, ...],
    count: 50
}
```
- `count` field is used for pagination purpose on frontend
- Debounce of 500ms to prevent execessive API calls

### User Story 3: CRUD

Validations:

- `id` and `login` should be alphanumeric and unique
- `salary` should be between `0.00` and `99999999.99`

Assumptions:

- PATCH request does not require all column to be present
- PATCH request will only update column present in request body
- `id` and `login` to be stored in lowercase
- `salary` to drop any digit after second decimal place

Note:

- Frontend is responsive
- All changes made on frontend will persist in backend (create, update and delete)
- If json payload is invalid, HTTP 400 status is returned
- If user is created successfully, HTTP 201 status is returned
- If user is deleted successfully, HTTP 204 status is returned
- If user is updated successfully, HTTP 200 status is returned
- If duplicated `id` or `login`, HTTP 409 status is returned
- if user is not found, HTTP 404 status is returned
- `id` in body is ignored for PATCH request

### User Story 4: Better UX When Uploading Large CSV Files

Note:

- Frontend will display loading screen when processing file
- Frontend will display the approriate messages when
    - File uploaded successfully
    - File is invalid
    - Another file is being processed

### User Story 5: UI Localization

Note:

- Required to refresh page after changing language
- UI fallbacks to English

## Contact

- **Alvin Choong** - [alvinc.me](https://alvinc.me/) - [alvin.choong92@gmail.com](mailto:alvin.choong92@gmail.com)
- Project Link: [https://bitbucket.org/alvinchoong/govtech-techhunt](https://bitbucket.org/alvinchoong/govtech-techhunt)