import React from 'react';
import PropTypes from 'prop-types';
import { Trans } from "react-i18next";

import NumberFormat from "react-number-format";

import { makeStyles } from '@material-ui/core/styles';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TablePagination from '@material-ui/core/TablePagination';
import TableRow from '@material-ui/core/TableRow';
import TableSortLabel from '@material-ui/core/TableSortLabel';
import Paper from '@material-ui/core/Paper';
import IconButton from "@material-ui/core/IconButton";
import EditIcon from '@material-ui/icons/Edit';
import DeleteIcon from '@material-ui/icons/Delete';

const headCells = [
  { id: 'id', align: 'left' },
  { id: 'name', align: 'left' },
  { id: 'login', align: 'left' },
  { id: 'salary', align: 'right' },
];

function CustomTableHead(props) {
  const { classes, order, orderBy, onRequestSort } = props;

  const createSortHandler = (property) => (event) => {
    onRequestSort(event, property);
  };

  return (
    <TableHead>
      <TableRow>
        {headCells.map((headCell) => (
          <TableCell
            key={headCell.id}
            align={headCell.align}
            padding={'default'}
            sortDirection={orderBy === headCell.id ? order : false}
          >
            <TableSortLabel
              active={orderBy === headCell.id}
              direction={orderBy === headCell.id ? order : 'asc'}
              onClick={createSortHandler(headCell.id)}
            >
              <Trans>{headCell.id}</Trans>

              {orderBy === headCell.id ? (
                <span className={classes.visuallyHidden}>
                  {order === 'desc' ? 'sorted descending' : 'sorted ascending'}
                </span>
              ) : null}
            </TableSortLabel>
          </TableCell>
        ))}
        <TableCell align="right">
          <Trans>action</Trans>
        </TableCell>
      </TableRow>
    </TableHead>
  );
}

CustomTableHead.propTypes = {
  classes: PropTypes.object.isRequired,
  onRequestSort: PropTypes.func.isRequired,
  order: PropTypes.oneOf(['asc', 'desc']).isRequired,
  orderBy: PropTypes.string.isRequired,
};

const useStyles = makeStyles((theme) => ({
  root: {
    width: '100%',
  },
  paper: {
    width: '100%',
    marginBottom: theme.spacing(2),
  },
  table: {
    minWidth: 750,
  },
  visuallyHidden: {
    border: 0,
    clip: 'rect(0 0 0 0)',
    height: 1,
    margin: -1,
    overflow: 'hidden',
    padding: 0,
    position: 'absolute',
    top: 20,
    width: 1,
  },
}));

export default function EnhancedTable(props) {
  const classes = useStyles();

  const {
    data,
    queryData,
    updateQueryData,
    setModalId,
    setModalState,
    setFormData,
  } = props;
  const { order, orderBy, page, rowsPerPage } = queryData;

  const editUser = (i) => {
    const user = data.results[i];
    setModalId(user.id);
    setModalState("edit");
    setFormData(user);
  }

  const deleteUser = (i) => {
    const user = data.results[i];
    setModalId(user.id);
    setModalState("delete");
  };

  const handleRequestSort = (event, property) => {
    const isAsc = orderBy === property && order === "asc";
    updateQueryData({
      order: isAsc ? "desc" : "asc",
      orderBy: property
    });
  };

  const handleChangeRowsPerPage = (event) => {
    updateQueryData({
      rowsPerPage: parseInt(event.target.value, 10),
      page: 0
    });
  };

  const handleChangePage = (event, newPage) => {
    updateQueryData({
      page: newPage,
    });
  };

  return (
    <div className={classes.root}>
      <Paper className={classes.paper}>
        <TableContainer>
          <Table
            className={classes.table}
            aria-labelledby="tableTitle"
            size={"small"}
            aria-label="user table"
          >
            <CustomTableHead
              classes={classes}
              order={queryData.order}
              orderBy={queryData.orderBy}
              onRequestSort={handleRequestSort}
            />
            <TableBody>
              {data.results.map((row, i) => {
                return (
                  <TableRow hover tabIndex={-1} key={row.name}>
                    <TableCell>{row.id}</TableCell>
                    <TableCell>{row.name}</TableCell>
                    <TableCell>{row.login}</TableCell>
                    <TableCell align="right">
                      <NumberFormat
                        value={row.salary}
                        displayType={"text"}
                        prefix={"S$"}
                        decimalScale={2}
                        fixedDecimalScale={true}
                        thousandSeparator={true}
                      />
                    </TableCell>
                    <TableCell align="right">
                      <IconButton aria-label="edit" onClick={() => editUser(i)}>
                        <EditIcon />
                      </IconButton>
                      <IconButton aria-label="delete" onClick={() => deleteUser(i)}>
                        <DeleteIcon />
                      </IconButton>
                    </TableCell>
                  </TableRow>
                );
              })}
            </TableBody>
          </Table>
        </TableContainer>
        <TablePagination
          rowsPerPageOptions={[5, 10, 20, 30]}
          component="div"
          count={data.count}
          rowsPerPage={rowsPerPage}
          page={page}
          onChangePage={handleChangePage}
          onChangeRowsPerPage={handleChangeRowsPerPage}
          labelRowsPerPage={<Trans>limit</Trans>}
        />
      </Paper>
    </div>
  );
}
