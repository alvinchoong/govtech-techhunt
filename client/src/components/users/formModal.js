import React from "react";
import { useTranslation } from "react-i18next";

import NumberFormat from "react-number-format";
import TextField from "@material-ui/core/TextField";

import ModalLayout from "../template/modalLayout";

const FormModal = (props) => {
  const { t } = useTranslation();
  const { modalState, formData, setFormData } = props;
  const state = modalState ? modalState.charAt(0).toUpperCase() + modalState.slice(1) : "";
  
  const handleChange = (e) => {
    const newData = {...formData};
    const {name, value} = e.target;
    newData[name] = value;
    setFormData(newData);
  }

  const handleSalaryChange = ({ value }) => {
    const newData = { ...formData };
    newData.salary = value;
    setFormData(newData);
  }

  const inputProps = {
    variant: 'outlined',
    fullWidth: true,
    margin: 'normal'
  }

  return (
    <ModalLayout heading={`${t(state)} ${t('employee')}`} {...props}>
      <TextField
        id="id"
        name="id"
        label={t('id')}
        value={formData.id}
        onChange={handleChange}
        {...inputProps}
        disabled={modalState === "edit"}
      />
      <TextField
        id="name"
        name="name"
        label={t('name')}
        value={formData.name}
        onChange={handleChange}
        {...inputProps}
      />
      <TextField
        id="login"
        name="login"
        label={t('login')}
        value={formData.login}
        onChange={handleChange}
        {...inputProps}
      />
      <NumberFormat
        {...inputProps}
        id="salary"
        label={t('salary')}
        value={formData.salary}
        prefix={"S$"}
        decimalScale={2}
        fixedDecimalScale={true}
        thousandSeparator={true}
        customInput={TextField}
        onValueChange={handleSalaryChange}
      />
    </ModalLayout>
  );
};

export default FormModal;