import React from "react";
import { useTranslation } from "react-i18next";

import DialogContentText from "@material-ui/core/DialogContentText";
import ModalLayout from '../template/modalLayout';

const ConfirmationModal = (props) => {
  const { t } = useTranslation();

  return (
    <ModalLayout heading={t("confirmation")} {...props}>
      <DialogContentText>{t("confirm_delete_employee")}</DialogContentText>
    </ModalLayout>
  );
};

export default ConfirmationModal;
