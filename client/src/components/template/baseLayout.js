import React from 'react';

import { makeStyles } from '@material-ui/core/styles';
import CssBaseline from '@material-ui/core/CssBaseline';
import Grid from '@material-ui/core/Grid';

import Nav from '../common/nav';
import Header from '../common/header';

const useStyles = makeStyles((theme) => ({
  root: {
    [theme.breakpoints.between("md", "xl")]: {
      display: "flex",
    },
  },
  toolbar: theme.mixins.toolbar,
  content: {
    flexGrow: 1,
    padding: theme.spacing(3),
  },
}));

const BaseLayout = (props) => {
  const {heading, children} = props;

  const classes = useStyles();
  const [mobileOpen, setMobileOpen] = React.useState(false);

  const handleDrawerToggle = () => {
    setMobileOpen(!mobileOpen);
  };

  return (
    <div className={classes.root}>
      <CssBaseline />
      <Header heading={heading} toggleMenu={handleDrawerToggle} />
      <Nav open={mobileOpen} onClose={handleDrawerToggle} />

      <main className={classes.content}>
        <div className={classes.toolbar} />
        <Grid container spacing={3}>
          {children}
        </Grid>
      </main>
    </div>
  );
}

export default BaseLayout;