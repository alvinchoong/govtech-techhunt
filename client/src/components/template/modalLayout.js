import React from "react";
import { useTranslation } from "react-i18next";

import Button from "@material-ui/core/Button";
import Dialog from "@material-ui/core/Dialog";
import DialogActions from "@material-ui/core/DialogActions";
import DialogContent from "@material-ui/core/DialogContent";
import DialogTitle from "@material-ui/core/DialogTitle";
import Alert from "@material-ui/lab/Alert";
import AlertTitle from "@material-ui/lab/AlertTitle";

const ModalLayout = (props) => {
  const { t } = useTranslation();
  const {
    open, 
    heading,
    handleClose,
    handleSubmit,
    children
  } = props;

  const errors = props.errors || [];

  const displayErrors = () => {
    return (
      errors.length ? 
      <Alert variant="outlined" severity="error">
        <AlertTitle>Error</AlertTitle>
        <ul style={{ margin: 0, padding: 0, listStyle: "none" }}>
          {errors.map((err, i) => (<li key={i}>{err}</li>))}
        </ul>
      </Alert> : ""
    );
  }
  
  return (
    <Dialog
      open={open}
      onClose={handleClose}
      aria-labelledby="dialog-title"
      aria-describedby="dialog-description"
    >
      <DialogTitle id="dialog-title">{heading}</DialogTitle>
      <DialogContent>
        {children}
        {displayErrors()}
      </DialogContent>
      <DialogActions>
        <Button onClick={handleClose} color="primary">
          {t("cancel")}
        </Button>
        <Button onClick={handleSubmit} color="primary" autoFocus>
          {t("confirm")}
        </Button>
      </DialogActions>
    </Dialog>
  );
}

export default ModalLayout;