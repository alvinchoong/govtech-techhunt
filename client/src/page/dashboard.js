import React, { useState, useEffect } from "react";

import { useTranslation } from "react-i18next";
import axios from "axios";

import NumberFormat from "react-number-format";
import { makeStyles } from "@material-ui/core/styles";
import Grid from "@material-ui/core/Grid";
import Fab from "@material-ui/core/Fab";
import TextField from "@material-ui/core/TextField";
import AddIcon from "@material-ui/icons/Add";

import BaseLayout from "../components/template/baseLayout";
import UserTable from '../components/users/userTable';
import UserModal from "../components/users/formModal";
import ConfirmationModal from "../components/users/confirmationModal";
import LoadingBackdrop from "../components/common/loadingBackdrop";
import Alert from "../components/common/alert";

const defaultData = {
  results: [],
  count: 0
}

const defaultQueryData = {
  minSalary: 0,
  maxSalary: 10000,
  order: 'asc',
  orderBy: 'id',
  page: 0,
  rowsPerPage: 5
}

const defaultFormData = {
  id: "",
  name: "",
  login: "",
  salary: 0,
};

const defaultAlertData = {
  open: false,
  type: null,
  message: null,
};

const baseEndpoint = "http://localhost:8000/users";

const useStyles = makeStyles((theme) => ({
  fab: {
    position: "fixed",
    bottom: theme.spacing(2),
    right: theme.spacing(2),
  },
  tableGrid: {
    marginBottom: theme.spacing(5)
  },
}));

const Dashboard = () => {
  const { t } = useTranslation();
  const [data, setData] = useState(defaultData); 
  const [queryData, setQueryData] = useState(defaultQueryData);
  const [modalId, setModalId] = useState(null);
  const [modalState, setModalState] = useState(null);
  const [modalErrors, setModalErrors] = useState([]);
  const [formData, setFormData] = useState(defaultFormData);
  const [alertData, setAlertData] = useState(defaultAlertData);
  const [loading, setLoading] = useState(false);

  const classes = useStyles();

  const updateQueryData = (obj) => {
    const newQueryData = { ...queryData };
    for(const k in obj) {
      newQueryData[k] = obj[k];
    }
    setQueryData(newQueryData);
  };

  const handleModalSubmit = () => {
    setLoading(true);
    // do update or create user
    let method = "post";
    let endpoint = baseEndpoint;

    if (modalState === "edit") {
      method = "patch";
      endpoint += `/${modalId}`;
    }

    axios({
      method: method,
      url: endpoint,
      data: formData,
    }).then((res) => {
      let message;
      if(method === "post") {
        message = t("employee_create_sucessful");
      } else {
        message = t("employee_update_sucessful");
      }
      ;
      resetModal();
      fetchUsers();
      openAlert("success", message);
    }).catch((err) => {
      console.log(err);
      let errors = [];
      if (err.response) {
        if (err.response.data.errors) {
          // managed errors (array) e.g validation
          errors = err.response.data.errors;
        } else {
          // server error
          errors = [err.response.statusText];
        }
      } else {
        // everything else e.g network error
        errors = [err.message];
      }
      setModalErrors(errors);
    }).finally(() => {
      setLoading(false);
    });
  };

  const fetchUsers = () => {
    setLoading(true);

    const endpoint = `${baseEndpoint}?${buildQueryString()}`
    axios.get(endpoint).then((res) => {
      setData(res.data);
    }).catch((err) => {
      console.log(err);
      let message;
      if (err.response) {
        if (err.response.data.errors) {
          // managed errors (array) e.g validation
          message = err.response.data.errors[0];
        } else {
          // server error
          message = err.response.statusText;
        }
      } else {
        // everything else e.g network error
        message = err.message;
      }
      openAlert("error", message);
    }).finally(() => {
      setLoading(false);
    });
  };

  const deleteUser = () => {
    setLoading(true);

    const endpoint = `${baseEndpoint}/${modalId}`;
    axios.delete(endpoint).then((res) => {
      resetModal();
      fetchUsers();
      openAlert("success", t("employee_delete_sucessful"));
    }).catch((err) => {
      resetModal();
      openAlert("error", err.message);
    }).finally(() => {
      setLoading(false);
    });
  };

  const openAlert = (type, message) => {
    setAlertData({
      open: true,
      type,
      message,
    });
  }

  const resetModal = () => {
    setFormData(defaultFormData);
    setModalId(null);
    setModalState(null);
    setModalErrors([]);
  };

  const buildQueryString = () => {
    const {minSalary, maxSalary, order, orderBy, page, rowsPerPage} = queryData;
    let sortSign = "";
    if (order === "asc") sortSign = "+";
    else if (order === "desc") sortSign = "-";

    const params = {
      minSalary,
      maxSalary,
      offset: page * rowsPerPage,
      limit: rowsPerPage,
      sort: `${sortSign}${orderBy}`,
    };

    const queryString = Object.keys(params)
      .map((key) => key + "=" + params[key])
      .join("&");

    return queryString;
  };
  
  useEffect(() => {
    // add debounce
    const timer = setTimeout(() => fetchUsers(), 500);
    return () => clearTimeout(timer);
  }, [queryData]);

  const tableOptions = {
    data,
    queryData,
    updateQueryData,
    setModalId,
    setModalState,
    setFormData
  };

  const inputProps = {
    variant: "outlined",
    fullWidth: true,
    InputLabelProps: {
      shrink: true,
    },
    prefix: "S$",
    type: "text",
    decimalScale: 2,
    fixedDecimalScale: true,
    thousandSeparator: true,
    customInput: TextField,
  };

  return (
    <BaseLayout heading="dashboard">
      <Grid item xs={12} sm={6}>
        <NumberFormat
          {...inputProps}
          label={t("min_salary")}
          value={queryData.minSalary}
          name={"minSalary"}
          onValueChange={({ value }) => updateQueryData({ minSalary: value })}
        />
      </Grid>
      <Grid item xs={12} sm={6}>
        <NumberFormat
          {...inputProps}
          label={t("max_salary")}
          value={queryData.maxSalary}
          name={"maxSalary"}
          onValueChange={({ value }) => updateQueryData({ maxSalary: value })}
        />
      </Grid>

      <Grid item xs={12} className={classes.tableGrid}>
        <UserTable {...tableOptions} />
      </Grid>

      <Grid item xs={12}>
        <Fab
          color="primary"
          aria-label="add"
          className={classes.fab}
          onClick={() => setModalState("add")}
        >
          <AddIcon />
        </Fab>
      </Grid>

      <UserModal
        modalState={modalState}
        open={modalState === "edit" || modalState === "add"}
        handleClose={resetModal}
        handleSubmit={handleModalSubmit}
        formData={formData}
        setFormData={setFormData}
        errors={modalErrors}
      />

      <ConfirmationModal
        open={modalState === "delete"}
        handleClose={resetModal}
        handleSubmit={deleteUser}
      />

      <Alert alertData={alertData} closeAlert={() => setAlertData(defaultAlertData)} />
      <LoadingBackdrop open={loading} />
    </BaseLayout>
  );
}

export default Dashboard;