import React, { useCallback, useState } from "react";

import { useTranslation } from "react-i18next";
import { useDropzone } from "react-dropzone";
import axios from "axios";

import Grid from '@material-ui/core/Grid';
import Paper from '@material-ui/core/Paper';
import Button from '@material-ui/core/Button';
import IconButton from "@material-ui/core/IconButton";
import CloudUploadIcon from "@material-ui/icons/CloudUpload";
import { makeStyles } from "@material-ui/core/styles";

import List from "@material-ui/core/List";
import ListItem from "@material-ui/core/ListItem";
import ListItemAvatar from "@material-ui/core/ListItemAvatar";
import ListItemSecondaryAction from "@material-ui/core/ListItemSecondaryAction";
import ListItemText from "@material-ui/core/ListItemText";
import Avatar from "@material-ui/core/Avatar";
import AttachFileIcon from "@material-ui/icons/AttachFile";
import DeleteIcon from "@material-ui/icons/Delete";

import BaseLayout from "../components/template/baseLayout";
import LoadingBackdrop from "../components/common/loadingBackdrop";
import Alert from "../components/common/alert";

const useStyles = makeStyles((theme) => ({
  wrapper: {
    marginTop: theme.spacing(1),
    position: "relative",
  },
  dropzone: {
    height: "400px",
    lineHeight: "400px",
    textAlign: "center",
    border: "dashed #8e8e8e",
  },
  success: {
    backgroundColor: "#4caf50",
  },
  error: {
    backgroundColor: "#f44336",
  },
}));

const defaultAlertData = {
  open: false,
  type: null,
  message: null
}

const UserUpload = () => {
  const { t } = useTranslation();
  const classes = useStyles();
  const [isLoading, setIsLoading] = useState(false);
  const [alertData, setAlertData] = useState(defaultAlertData);
  const [file, setFile] = useState(null);

  const onDrop = useCallback((acceptedFiles) => {
    setFile(acceptedFiles[0]);
  }, []);

  const uploadFile = () => {
    setIsLoading(true);

    const data = new FormData();
    data.append("file", file);

    axios
      .post("http://localhost:8000/users/upload", data)
      .then((res) => {
        openAlert("success", t("file_upload_success"));
        setFile(null);
      })
      .catch((err) => {
        console.log(err);
        let message;
        if (err.response) {
          if (err.response.data.errors) {
            // managed errors (array) e.g validation
            message = err.response.data.errors[0];
          } else {
            // server error
            message = err.response.statusText;
          }
        } else {
          // everything else e.g network error
          message = err.message;
        }
        openAlert("error", message);
      }).finally(() => {
        setIsLoading(false);
      });
  }
  
  const openAlert = (type, message) => {
    setAlertData({
      open: true,
      type,
      message
    })
  }

  const closeAlert = () => {
    setAlertData(defaultAlertData);
  }

  const { getRootProps, getInputProps } = useDropzone({
    onDrop,
    accept: "text/csv",
    multiple: false,
  });

  return (
    <BaseLayout heading="upload">
      <Grid item xs={12}>
        <Paper {...getRootProps()} className={classes.dropzone}>
          <input {...getInputProps()} />
          {file ? t("dropzone_message_submit") : t("dropzone_message_upload")}
        </Paper>

        <div className={classes.wrapper}>
          <Button
            variant="contained"
            fullWidth={true}
            color="primary"
            disabled={isLoading}
            onClick={uploadFile}
            startIcon={<CloudUploadIcon />}
          >
            {t("upload")}
          </Button>
        </div>
      
        {file && (
          <List>
            <ListItem>
              <ListItemAvatar>
                <Avatar>
                  <AttachFileIcon />
                </Avatar>
              </ListItemAvatar>
              <ListItemText primary={file.name} />
              <ListItemSecondaryAction>
                <IconButton
                  edge="end"
                  aria-label="delete"
                  onClick={() => setFile(null)}
                >
                  <DeleteIcon />
                </IconButton>
              </ListItemSecondaryAction>
            </ListItem>
          </List>
        )}
      </Grid>

      <Alert alertData={alertData} closeAlert={closeAlert} />
      <LoadingBackdrop open={isLoading} />
    </BaseLayout>
  );
}

export default UserUpload;