import React, { useEffect } from "react";

import {
  BrowserRouter as Router,
  Switch,
  Route
} from "react-router-dom";

import i18n from "./i18n";

import Nav from './components/common/nav';
import Dashboard from './page/dashboard';
import UserUpload from './page/userUpload';

function App() {

  useEffect(() => {
    const userLang = navigator.language || navigator.userLanguage;
    i18n.changeLanguage(userLang);
  })

  return (
    <Router>
      <div>
        <Nav />

        <Switch>
          <Route path="/upload" >
            <UserUpload />
          </Route>
          <Route path="/">
            <Dashboard />
          </Route>
        </Switch>
      </div>
    </Router>
  );
}

export default App;